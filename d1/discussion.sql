-- inserting records/rows
INSERT INTO artists (name) VALUES ("BlackPink");
INSERT INTO artists (name) VALUES ("ITZY");

INSERT INTO albums (album_title, date_released, artist_id)
	VALUES(
		"Psy 6",
		"2020-10-2",
		1
	);

INSERT INTO albums (album_title, date_released, artist_id)
	VALUES(
		"Trip",
		"2020-10-2",
		1
	);

INSERT INTO albums (album_title, date_released, artist_id)
	VALUES(
		"The Album",
		"2020-10-2",
		1
	);

INSERT INTO albums (album_title, date_released, artist_id)
	VALUES(
		"Crazy In Love",
		"2021-9-24",
		2
	);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Love Sick Girls",
	313,
	"K-Pop",
	1
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"How You Like That",
	301,
	"K-Pop",
	1
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Pretty Savage",
	319,
	"K-Pop",
	1
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"LOCO",
	312,
	"K-Pop",
	2
);

-- [SECTION] Selecting Records

-- Display all columns for all songs
SELECT * FROM songs;

-- Display only the title and genre of all songs
SELECT song_name, genre FROM songs;

-- Display only the title of all OPM songs
SELECT song_name FROM songs WHERE genre = "K-Pop";

-- Display the title and length that are more than 4 mins
SELECT song_name, length FROM songs WHERE length > 240 AND genre = "K-Pop";

-- we can use both the AND and OR keywords for multiple expressions in the WHERE clause

-- [SECTION] Updating Records

-- Update the length
UPDATE songs SET length = 220 WHERE song_name = "LOCO";

-- omitting the WHERE clause will update ALL rows

-- [SECTION] Deleting songs

-- 
DELETE FROM songs WHERE genre = "K-Pop" AND length > 315;