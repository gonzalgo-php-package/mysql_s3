-- [ACTIVITY 1]

-- [ADD USERS]

INSERT INTO users (email, password, datetime_created) VALUES (
	"johnsmith@gmail.com",
	"passwordA",
	"2021-01-01 01:00:00"
);

INSERT INTO users (email, password, datetime_created) VALUES (
	"juandelacruz@gmail.com",
	"passwordB",
	"2021-01-01 02:00:00"
);

INSERT INTO users (email, password, datetime_created) VALUES (
	"janessmith@gmail.com",
	"passwordC",
	"2021-01-01 03:00:00"
);

INSERT INTO users (email, password, datetime_created) VALUES (
	"mariadelacruz@gmail.com",
	"passwordD",
	"2021-01-01 04:00:00"
);

INSERT INTO users (email, password, datetime_created) VALUES (
	"johndoe@gmail.com",
	"passwordE",
	"2021-01-01 05:00:00"
);

-- [ADD POSTS]

INSERT INTO posts (title, content, datetime_posted, author_id) VALUES (
	"First Code",
	"Hello World!",
	"2021-01-02 01:00:00",
	11
);

INSERT INTO posts (title, content, datetime_posted, author_id) VALUES (
	"Second Code",
	"Hello Earth!",
	"2021-01-02 02:00:00",
	11
);

INSERT INTO posts (title, content, datetime_posted, author_id) VALUES (
	"Third Code",
	"Welcome to Mars!",
	"2021-01-02 03:00:00",
	12
);

INSERT INTO posts (title, content, datetime_posted, author_id) VALUES (
	"Fourth Code",
	"Bye bye solar system!",
	"2021-01-02 04:00:00",
	14
);

-- Get all the post with an Author ID of 11
SELECT * FROM posts WHERE author_id = 11;

-- Get all the user's email and datetime of creation
SELECT email, datetime_created FROM users;

-- Update a post's content to "Hello to the People of the Earth!" where its initial content is "Hello Earth!" by using the record's ID.
UPDATE posts SET content = "Hello to the People of the Earth!" WHERE id = 2; 

-- Delete the user with an email of "johndoe@gmail.com"
DELETE FROM users WHERE email = "johndoe@gmail.com";